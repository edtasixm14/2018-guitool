# Documentacion para usuarios

## Que es GUIT i para que sirve.

La idea principal de “GUI-tool for System Linux Administration” es la de ofrecer una herramienta gráfica que permita a los administradores de sistemas Linux (en una primera versión sólo per Fedora) realizar algunas de las acciones de administracion local de manera sencilla, rápida y ágil gracias a una plataforma web adaptada para este fin.

## Herramientas disponibles

A continuación se presentan las herramientas que un usuario puede utilizar de momento con la aplicación. 

### Creación de usuarios

Para crear un usuario local del sistema el administrador rellena un simple formulario web con los datos del nuevo usuario que desea crear. Este formulario esta formado por los siguientes campos:
- **nombre** (obligatorio): Indica el nombre que tendrá el usuario.
- **contraseña** (obligatorio): Establece la *password* que tendrá del usuario.
- **contraseña2** (obligatorio): Vuelve a establecer la *password* que tendrá el usuario.
- **grupo principal**: Indica el grupo principal al que pertenecerá el usuario.
- **grupos secundarios**: Indica los grupos secundarios a los que pertenecerá el usuario.
- **homedir**: Indica la ruta absoluta del sistema de ficheros donde se ubicará el directorio *home* del usuario. 
- **nombre**: Indica el nombre completo que tendrá asignado el usuario.
- **dirección**: Indica la dirección que tendrá asignado el usuario.
- **número de teléfono**: Indica el numero de telefono que tendrá asignado el usuario.

Previamente a la ejecución, el sistema realiza una serie de comprovaciones para asegurarse que el usuario podrá crearse. La comprovación de errores implementada hasta la fecha es la siguiente:
- Que se hayan introducido todos los campos obligatorios.
- Que el sistema no dispone ya de este usuario.
- Que los dos passwords introducidos coincidan.
- Que la ruta del directorio home sea una ruta correcta del sistema.

Si se selecciona el modo *default* para el grupo principal este usuario pertenecera a un grupo que se llamará igual que como su nombre. Además, si se selecciona el modo *default* para el directorio *home*, éste será ubicado dentro de `/home`, en una carpeta que se llamará como su nombre.

### Creación de rutinas de backups

Para crear una rutina de copia de seguridad de un directorio en concreto, el administrador rellena un formulario que está formado por los siguientes campos:
- **ruta**: Indica la ruta absoluta del sistema de ficheros del directorio que se desea crear la rutina de backup.
- **regularidad**: Se especifica cada cuanto tiempo se desea hacer la copia de seguridad del directorio en cuestion.
- **ahora**: Indica si se desea crear el primer backup ahora mismo.

Previamente a la ejecución, el sistema realiza una serie de comprovaciones para asegurarse que el la rutina podrá crearse. La comprovación de errores implementada hasta la fecha es la siguiente:
- Que se hayan introducido todos los campos obligatorios.
- Que exista el directorio sobre el que se quiere crear la rutina.
- Que no exista una rutina identica a la introducida.

Una vez enviado el formulario se crea la rutina en el sistema. 
Todos los backups se guardan en un directorio creado durante la instalación. Éste directorio se llama *Backups_guitool* y se encuentra en el *home* del usuario root.

### Creación de containers Docker

Para crear un container de Docker con caracteristicas específicas, el administrador rellena un formulario que esta formado por los siguientes campos:
- **base** (obligatorio): Indica la imagen base sobre la que se formará el container.
- **nombre** (obligatorio): Indica el nombre con el que será llamado el container.
- **hostname** (obligatorio): Indica el hostname que tendrá asignado el container.
- **subnet**: Selecciona cual sera la red de la que el container formara parte.
- **paquetes**: Indica el paquete (o paquetes) que se desea que el container traiga por defecto instalados. 
- **directorio**: Indica cual será la ruta absoluta del container sobre el que queremos empezar a trabajar.
- **otras**: Establece otras opciones del container (*privileged* y ficheros, detalladas más adelante).

Previamente a la ejecución, el sistema realiza una serie de comprovaciones para asegurarse que el container de docker puede crearse. La comprovación de errores implementada hasta la fecha es la siguiente:
- Que se hayan introducido todos los campos obligatorios.
- Que docker esté instalado y se pueda arrancar.
- Que la ruta del directorio de trabajo introducida sea una ruta valida del sistema (no necesita existir)
- Que exista la imagen a partir de la cual se quiere crear el docker.
- Que no exista otro docker con este mismo hostname o nombre. 

Si se selecciona la opcion "otra" y, por tanto, se indica una base personalizada sobre la cual crear el container solo se podran instalar los paquetes que posteriormente se seleccionen si ésta base personalizada es de alguna variante a las bases de los sistemas que vienen por defecto (fedora, ubuntu, opensuse, debian). De este modo, el usuario puede crear por ejemplo un container con paquetes instalados por defecto de la version que desee de fedora (fedora:24).

Las redes sobre las cuales el docker se puede unir son las que tiene docker instaladas en el momento de la instanciación de la pagina. Si se desea que el docker forme parte de una nueva subnet que no aparece en el listado se tiene que crear previamente y de forma manual mediante linea de comandos. Ejemplo: `docker network create --subnet x.x.x.x --gateway x.x.x.x`. Ver documentacion [network create](https://docs.docker.com/engine/reference/commandline/network_create/#options) para mas opciones. 

Actualmente las herramientas que se instalan por defecto en el container a partir de los diferentes packs que se pueden seleccionar son los siguientes:
- **Herramientas basicas**: vim, procps, mlocate, man-db, tree
- **Herramientas de redes**: nmap, iputils, iproute, telnet
- **Paquete de SSH**: openssh (server y clients)
- **Paquete de XinetD**.

Si no se especifica un directorio de trabajo concreto automáticamente éste será `/opt/docker`. Por contrario, si se especifica una ruta especifica de directorio de trabajo y no existe en el sistema de ficheros de container esta se creará siempre que sea una ruta valid de sistemas UNIX.

Por el momento, las otras opciones que se permiten especificar para modificar las caracteristicas del container son las siguientes:
- **files**: Abre un popup que permite seleccionar los diferentes ficheros que se deseen incorporar en el container. Estos ficheros siempre seran copiados en el directorio de trabajo del destino.
- **privileged**: Establece si el container tiene la opcion privileged activada o desactivada. 

Una vez enviado el formulario se abre una instancia de terminal con el container docker. Por defecto cuando hacemos un exit del container (^d) éste se elimina del sistema. Aún asi, si deseamos dejar el container abierto solo tenemos que cerrar la ventana del terminal. Para volver a entrar tendremos que ejecutar la siguiente orden en algun otro terminal: `docker exec -it containername /bin/bash`. 


### Creació de bases de datos pgsql
Para crear una BBDD de psql, el administrador rellena un formulario que esta formado por las siguientes secciones:

- Seccion 1: *Nombre de la BBDD. Dispone de los siguientes campos y botones*:
	- **nombre** (obligatorio): Permite establecer el nombre con el que sera creada la bbdd.
	- **Boton borrar todo**: Permite limpiar todo el formulario de datos para poder empezar desde zero con una nueva creacion.
- Seccion 2: *Añade/Borra tablas. Dispone de los siguientes campos y botones*:
	- **nombre** (obligatorio): Permite establecer el nombre de la nuvea tabla a crear.
	- **campos** (obligatorio): Permite establecer, mediante espacio como separador, los campos que formaran la tabla.
	- **Boton Añadir tabla**: Recoje la informacion de los dos campos anteriores y crea la tabla.
	- **Boton Borrar tabla**: Recoje la informacion del campo "nombre" y borra la tabla especificada.
- Seccion 3: *Modificar campos. Dispone de los siguientes campos y botones*:
	- **nombre**: Campo no modificable que indica el nombre del campo que estamos editando.
	- **tipo**: Permite especificar de que tipo de dato sera dicho campo.
	- **pk**: Permite establecer si este campo es una clave primaria.
	- **unique**: Permite establecer si este campo contendra valores no repetidos.
	- **not null**: Permite establecer si este campo contendra valores nulos o no.
	- **fk**: check que activa un selector que permite indicar el campo al que este hará referencia como FK.
	- **Boton actualizar/guardar**: Procesa la informacion de todos los campos anteriores y los guarda en memoria por si se desea comprobar que la informacion enviada al siguiente paso es la correcta o por si se desea cambiar de herramienta o, sobretotdo, por si se desea actualizar la informacion para poder establecer las foreign keys (se recomienda hacer al final).
- Seccion 4: Crear la BBDD o el script. Dispone de los siguientes campos y botones:
	- **Boton obtener script**: Procesa la informacion de todas las secciones anteriores y lanza un pop-up preguntando donde se desea guardar el script de creacion de la BBDD que ha generado la herramienta. Util cuando se quiere revisar algo o implementar alguna opcion extra que la herramienta (aún) no permite, como por ejemplo claves primarias multiples.
	- **Boton crear bbdd**: Procesa la informacion de todas las secciones y lanza el script que crea la BBDD en el postgres del sistema.
	- **Campos user y password (obligatorio)**: Permite indicar el usuario y la contraseña del usuario de postgres con el cual se quiere crear la bbdd.

Previamente a la ejecucion, el sistema realiza una serie de comprovaciones para asegurarse que la bbdd puede crearse. La comprovacion de errores implementada hasta la fecha es la siguiente:
- Que se hayan introducido todos los campos obligatorios.
- Que el nombre de la BBDD o de los campos no sea una palabra reservada de psql y que no empiezen por numero.
- Que el nombre de una nueva tabla que se desea introducir no se haya introducido aún.
- Que una tabla solo pueda tener un campo con el mismo nombre. No pueden repetirse.
- Que este psql instalado y se pueda arrancar (no en el caso de obtener script).
- Que haya almenos un campo por tabla y que haya, como maximo, 50 por tabla.
- Previene que solo se puedan introducir foreign keys referenciadas a un campo que sea serial o clave primaria.

Una vez comprovados los siguientes errores se puede crear la BBDD. Aún asi, si salta algun tipo de error no controlado previamente durante la ejecucion automatica del script, un mensaje con estos errores sera mostrado en el apartado de alertas de la parte superior.

Si el nombre de la BBDD que se especifica ya existe en nuestro sistema, la herramienta se conectara a ésta. Esto podria haberse prohibido pero se deja habilitado ya que nos permite poder añadir algunas tablas nuevas a una BBDD ya existente (pese a que se visualizarà este error en pantalla, la tabla se habrà añadido). Importante: Estas nuevas tablas si que NO pueden existir. En tal caso, no se crearán.
