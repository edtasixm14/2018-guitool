from django.db import models

class Herramientas(models.Model):
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=1000)
    link = models.CharField(max_length=150, default='none')
    data_publicacio = models.DateField()

    def __str__(self):
        return self.titulo
